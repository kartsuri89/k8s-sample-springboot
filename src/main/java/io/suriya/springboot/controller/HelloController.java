package io.suriya.springboot.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@Value("${default.msg}")
	private String msg;
	
	@Value("${default.helm.msg}")
	private String helmMsg;
	
	@GetMapping("/")
	public ResponseEntity<String> check() {
		return new ResponseEntity<>(msg, HttpStatus.OK);
	}
	
	@GetMapping("/hello")
	public ResponseEntity<String> getHello() {
		return new ResponseEntity<>(helmMsg, HttpStatus.OK);
	}
	
}
